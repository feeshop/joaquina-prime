<?php include_once('header.php') ?>
<?php include_once('menu.php') ?>
<main class="intern">
	<section id="description" class="information">
		<div class="center-content">
			<h1>
				O <span>Joaquina Prime Kids</span> nasceu para resgatar
				o clima das festas infantis de antigamente…
			</h1>
			<div class="centered">
				<article>	
					<p>A emoção do dia do aniversário, quando toda a família se unia para encher os balões, abrir a forminha para o brigadeiro, decorar o bolo com confeitos e, por fim, receber os amigos para as brincadeiras.</p>
					<p>E este é o nosso convite: que o cliente transforme o Joaquina Prime Kids no salão de festas da sua casa. Uma estrutura única, ambiente inovador, sofisticado e confortável, se unem a diversão, tecnologia e segurança em momentos de muita alegria.</p>
					<p>
						Com um alto padrão de gastronomia e profissionais qualificados para atender os seus convidados de uma maneira personalizada e especial, e fazer da sua comemoração um momento inesquecível.</p>
					</article>
					<article>
						<p>Nossa arquitetura foi projetada para proporcionar total liberdade para que a criança e sua família possam personalizá-la enquanto nossa equipe especializada faz a perfeita tradução de cada sonho em realidade.</p>
						<p>O melhor Buffet da Zona Norte de São Paulo está de portas abertas para sua imaginação. </p>
						<p>Solicite um orçamento personalizado!</p>
						<a href="" class="btn-round">ORÇAMENTO</a>
					</article>
				</div>
			</div>
		</section>
		<section id="get-to-know-us" class="information">
			<div class="center-content">
				<h1>Conheça nossos ambientes</h1>
				<div class="centered">
					<p>Estamos estruturados para atender até 220 pessoas em um ambiente de 1.200m² moderno, aconchegante e alegre.</p>
					<p>O espaço possui área verde com jardim de inverno na parte externa, bar para os pais com mesa de bilhar e TV de 52", fraldário com trocador, poltrona de amamentação e cadeira de alimentação, cozinha show, pista de dança com iluminação, sanitários adaptados para deficientes físicos, ar condicionado,  ampla recepção com TV de plasma e Wi-fi.</p>
					<p><b>Confira nossos ambientes:</b></p>
					<div class="structure">
						<div class="type">
							<img src="images/reception.png" alt="Recepção preparada para receber sua comemoração!">
							<h2>RECEPÇÃO</h2>
							<p>
								Ampla recepção com TV de plasma. Contamos também com a contratação de uma recepcionista e um segurança.
							</p>
						</div>
						<div class="type">
							<img src="images/bar.png" alt="Bar bem equipado para melhor atendê-lo.">
							<h2>BAR</h2>
							<p>
								Bar totalmente equipado, com chopeira inteiramente disponível aos convidados. Além da disponibilidade da mesa de bilhar.
							</p>
						</div>
						<div class="type">
							<img src="images/forbabies.png" alt="Troque seu filho num lugar tranquilo e adequado.">
							<h2>FRALDÁRIO</h2>
							<p>
								Com trocador equipado para o bebê, poltrona de amamentação e cadeira de alimentação.
							</p>
						</div>
						<div class="type">
							<img src="images/green-area.png" alt="Jardins de inverno decorativos enriquecem o ambiente.">
							<h2>ÁREA VERDE</h2>
							<p>
								Além de charmosos, os Jardins de Inverno conferem ao ambiente um visual aconchegante e interessante junto à decoração.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="parties" class="information">
			<div class="center-content">
				<h1>Festas para todas as idades!</h1>
				<div class="centered">
					<p>Em nosso mundo o ambiente foi criado para atender festas de aniversário de todas as idades e a maioria dos nossos brinquedos fazem sucesso desde os pequenos até os adultos!</p>
				</div>
				<h1>Animação e bom gosto sempre passam por aqui</h1>
				<div class="centered">
					<p><a href="">Clique aqui</a> e confira algumas festas que já rolaram no Joaquina Prime Kids.</p>
				</div>
			</div>
		</section>
		<img src="images/kid-slide.png" alt="Garoto se divertindo em tobogã" class="kid-slide">
	</main>
	<?php include_once('footer.php') ?>
	<script>
	function listen() {
		if (window.innerWidth < 693) {
			$('.structure').slick({
				infinite: true, 
				autoplay: true, 
				autoplaySpeed: 2000,  
				arrows: false
			});
		}

		else{
			$('.structure').slick('unslick');
		}
	};

	listen();
	$(window).on('resize', listen)
	</script>