<nav class="fixed" role="menu">
	<button class="menu-item hamburger hamburger--3dy" role="menuitem" id="main" data-open="main" tabindex="1">
		<span class="hamburger-box">
			<span class="hamburger-inner"></span>
		</span>
		<svg xmlns="http://www.w3.org/2000/svg" width="62" height="183" viewBox="0 0 62 183">
			<filter id="filter" y="1" width="62" height="183" filterUnits="userSpaceOnUse">
				<feOffset result="offset" dy="3" in="SourceAlpha"/><feGaussianBlur result="blur"/>
				<feFlood result="flood" flood-opacity="0.2"/><feComposite result="composite" operator="in" in2="blur"/>
				<feBlend result="blend" in="SourceGraphic"/><feFlood result="flood-2" flood-color="#220237"/>
				<feComposite result="composite-2" operator="in" in2="SourceGraphic"/>
				<feBlend result="blend-2" in2="blend"/></filter>
				<path id="Shape" data-name="Shape" class="shape" d="M0 74V0S4.2 10.8 25 20 62 34 62 49v82c0 15-16.2 19.8-37 29S0 180 0 180V74Z"/>
			</svg>
		</button>
		<button class="menu-item" role="menuitem" id="wpp" tabindex="2" data-open="zap">
			<i class="fa fa-times" aria-hidden="true"></i>
			<i class="fa fa-whatsapp" aria-hidden="true"></i>
			<svg xmlns="http://www.w3.org/2000/svg" width="50" height="140" viewBox="0 0 62 183"><filter id="filter" y="1" width="62" height="183" filterUnits="userSpaceOnUse"><feOffset result="offset" dy="3" in="SourceAlpha"/><feGaussianBlur result="blur"/><feFlood result="flood" flood-opacity="0.2"/><feComposite result="composite" operator="in" in2="blur"/><feBlend result="blend" in="SourceGraphic"/><feFlood result="flood-2" flood-color="#220237"/><feComposite result="composite-2" operator="in" in2="SourceGraphic"/><feBlend result="blend-2" in2="blend"/></filter><path id="Shape" data-name="Shape" class="shape" d="M0 74V0S4.2 10.8 25 20 62 34 62 49v82c0 15-16.2 19.8-37 29S0 180 0 180V74Z"/>
			</svg>
		</button>
		<form action="https://www.facebook.com/buffetkidsjoaquinaprime/?fref=ts" target="_blank">	
		<button class="menu-item" role="menuitem" id="fb" tabindex="3">
			<i class="fa fa-facebook" aria-hidden="true"></i>
			<svg xmlns="http://www.w3.org/2000/svg" width="50" height="140" viewBox="0 0 62 183"><filter id="filter" y="1" width="62" height="183" filterUnits="userSpaceOnUse"><feOffset result="offset" dy="3" in="SourceAlpha"/><feGaussianBlur result="blur"/><feFlood result="flood" flood-opacity="0.2"/><feComposite result="composite" operator="in" in2="blur"/><feBlend result="blend" in="SourceGraphic"/><feFlood result="flood-2" flood-color="#220237"/><feComposite result="composite-2" operator="in" in2="SourceGraphic"/><feBlend result="blend-2" in2="blend"/></filter><path id="Shape" data-name="Shape" class="shape" d="M0 74V0S4.2 10.8 25 20 62 34 62 49v82c0 15-16.2 19.8-37 29S0 180 0 180V74Z"/>
			</svg>
		</button>
		</form>
		<form action="https://www.instagram.com/buffet_joaquinaprime/" target="_blank">	
		<button class="menu-item" role="menuitem" id="insta" tabindex="4">
			<i class="fa fa-instagram" aria-hidden="true"></i>
			<svg xmlns="http://www.w3.org/2000/svg" width="50" height="140" viewBox="0 0 62 183"><filter id="filter" y="1" width="62" height="183" filterUnits="userSpaceOnUse"><feOffset result="offset" dy="3" in="SourceAlpha"/><feGaussianBlur result="blur"/><feFlood result="flood" flood-opacity="0.2"/><feComposite result="composite" operator="in" in2="blur"/><feBlend result="blend" in="SourceGraphic"/><feFlood result="flood-2" flood-color="#220237"/><feComposite result="composite-2" operator="in" in2="SourceGraphic"/><feBlend result="blend-2" in2="blend"/></filter><path id="Shape" data-name="Shape" class="shape" d="M0 74V0S4.2 10.8 25 20 62 34 62 49v82c0 15-16.2 19.8-37 29S0 180 0 180V74Z"/>
			</svg>
		</button>
		</form>
		<button class="menu-item" role="menuitem" id="events" tabindex="5" data-open="primeEvents">
			<i class="fa fa-times" aria-hidden="true"></i>
			<img src="images/heart-ring.png" aria-hidden="true">
			<svg xmlns="http://www.w3.org/2000/svg" width="50" height="140" viewBox="0 0 62 183"><filter id="filter" y="1" width="62" height="183" filterUnits="userSpaceOnUse"><feOffset result="offset" dy="3" in="SourceAlpha"/><feGaussianBlur result="blur"/><feFlood result="flood" flood-opacity="0.2"/><feComposite result="composite" operator="in" in2="blur"/><feBlend result="blend" in="SourceGraphic"/><feFlood result="flood-2" flood-color="#220237"/><feComposite result="composite-2" operator="in" in2="SourceGraphic"/><feBlend result="blend-2" in2="blend"/></filter><path id="Shape" data-name="Shape" class="shape" d="M0 74V0S4.2 10.8 25 20 62 34 62 49v82c0 15-16.2 19.8-37 29S0 180 0 180V74Z"/>
			</svg>
		</button>
		<div class="open-menu">
			<img src="images/menu-lollipop.png" aria-hidden="true" class="menu-vector">
			<div class="instance" id="links" data-open="main">
				<ul>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>HOME</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>O BUFFET</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>ATRAÇÕES</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>CARDÁPIO</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>ROLOU AQUI</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>TIPOS DE FESTA</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>PARCEIROS</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>ORÇAMENTO</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>CONVITE VIRTUAL</a>
					</li>
					<li>
						<a href=""><i class="fa fa-arrow-right" aria-hidden="true"></i>CONTATO</a>
					</li>
				</ul>
			</div>
			<div class="instance" id="wpp-open" data-open="zap">
				<i class="fa fa-whatsapp" aria-hidden="true"></i>
				<p>
					Fale conosco também pelo
					<span>
						WhatsApp
					</span>
				</p>
				<p>
					11 94777.3922
				</p>
			</div>
			<div class="instance" id="prime-events" data-open="primeEvents">
				<p>Conheça também nossa segunda unidade para eventos em geral:</p>
				<img src="images/prime-event-logo.png" alt="Logotipo do espaço de eventos gerais - Joaquina Prime Eventos">
				<span>11 2218.0248</span>
				<p>Av. Joaquina Ramalho, 973
					Vila Guilherme - São Paulo/SP</p>
				</div>
				<div class="stripe"></div>
				<div class="number">	
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="240.341" height="65.03" viewBox="0 0 222.341 65.03">
						<path d="M186.921,0.029 C200.715,0.029 208.173,-0.786 216.562,9.802 C225.322,20.844 221.501,40.030 221.501,42.745 C221.501,45.461 221.035,54.239 214.511,58.494 C208.546,62.385 198.759,64.828 183.380,64.828 C179.932,64.828 33.195,65.462 23.781,64.377 C14.368,63.291 0.013,62.838 0.013,43.018 C0.013,28.537 1.691,13.332 5.790,7.450 C8.214,3.920 16.046,0.391 23.128,0.391 C24.340,0.391 181.049,0.029 186.921,0.029 Z" class="button"/>
					</svg>
					<p>11 2901.0204</p>
				</div>
				<p class="address">
					Av. Joaquina Ramalho, 1.071
					Vila Guilherme - São Paulo/SP
				</p>
			</div>
		</nav>