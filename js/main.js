// MENU 
// ------------------------------------

$('.menu-item').on('click', function(){
	var $this = $(this),
	$main = $('#main'),
	$openMenu = $('.open-menu'), 
	$zap = $('#wpp'), 
	$prime = $('#events'), 
	$mainLinks = $('#links'),
	$zapLinks = $('#wpp-open'),
	$eventLinks = $('#prime-events');

	if($this.is('.menu-item') && $this.hasClass('is-active')) {
		$this.parent('nav').animate({left : -280}, 400); 
		$this.removeClass('is-active'); 
		$('.instance').hide('slow');
		$('.stripe').css({
			'opacity' : '0'
		});
	}

	else {
		switch(true) {
			case ($this.is($main) || $this.parent('button').is($main)) :
			$openMenu.addClass('pink');
			$('.menu-item').removeClass('is-active');
			$this.addClass('is-active');
			$('.number, .address, .button').show();

			break; 

			case ($this.is($zap) || $this.parent('button').is($zap)) :
			$openMenu.removeClass('pink brown');
			$openMenu.addClass('green');
			$('.menu-item').removeClass('is-active');
			$this.addClass('is-active');
			$('.number, .address, .button').show();

			break; 

			case ($this.is($prime) || $this.parent('button').is($prime)) :
			$openMenu.removeClass('green pink');
			$openMenu.addClass('brown');
			$('.menu-item').removeClass('is-active');
			$this.addClass('is-active');
			$('.number, .address, .button').hide();

			break;
		}

		$this.parent('nav').animate({
			left : 0},
			400, function() {
				$('.instance').fadeOut(100);
				$('.instance[data-open="'+ $this.data('open') +'"]').show('slow');
			});

		$('.stripe').css({
			'opacity' : '1'
		});
	}
});


// HEADER 
//------------------------------------------

if ($('main').hasClass('intern')) {
	$('header').addClass('circled');
}


// LIGHTBOX
//------------------------------------------

$('#attractions button').on('click', function() {
	var $this = $(this),
	gallery = $this.data('gallery'),
	$thumbs = $('.contain ul');

	$thumbs.html('')

	$('.lightbox').fadeIn();
	$('html').addClass('noscroll');
	$this.addClass('selected')

	$('.contain h2').html(gallery[0]);
	$('.contain img').attr('src', gallery[1]);

	for (var i = 2;  i < gallery.length; i++) {
		$thumbs.append("<li class='thumbs'><img src='"+gallery[i]+"'/></li>"); 
	} 
});

$('.contain ul').delegate('li', 'click', function() {
	toShow = $(this).find('img').attr('src'), 
	gallery = $('#attractions button.selected').data('gallery');
	$('.picture img').fadeOut('fast').promise().done(function() {
		$(this).attr('src', toShow).fadeIn();
	});
});

$('.close').on('click', function() {
	$('.lightbox').fadeOut(); 
	$('html').removeClass('noscroll') ;
	$('#attractions button').removeClass('selected')
});

$(document).ready(function() {
	if ($('main').hasClass('intern')) {
		$('body').addClass('intern')
	}; 	
});

