
var gulp = require('gulp'), 
stylus = require('gulp-stylus'),
cmq = require('gulp-combine-media-queries');

//DEFAULT
//----------------------------------------------------------------------

gulp.task('default', ['stylus', 'watch']);

	
// COMPÌLE
//----------------------------------------------------------------------

gulp.task('stylus', function(){
	return gulp.src('css/stylus/*.styl')
	.pipe(stylus())
	.pipe(gulp.dest('css'));
});


//COMPRESS
//----------------------------------------------------------------------

gulp.task('compress', function () {
  return gulp.src('./css/stylus/*.styl')
    .pipe(stylus({
      compress: false
    }))
    .pipe(gulp.dest('./css'));
});

// WATCH
//----------------------------------------------------------------------

gulp.task('watch', function() {
	gulp.watch('css/stylus/**/*.styl', ['stylus', 'compress']);
});

//MEDIA QUERY COMBINER FOR PREPROCESSOR NESTING 
//----------------------------------------------------------------------

gulp.task('cmq', function() {
	gulp.src('css/main.css')
	.pipe(cmq({
		log: true
	}))
	.pipe(gulp.dest('css'))
}); 