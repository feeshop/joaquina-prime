<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="900" height="100" viewBox="0 0 62 183">
	<defs>
		<style>
		.cls-1 {
			fill: #220237;
			fill-rule: evenodd;
			filter: url(#filter);
		}
		</style>
		<filter id="filter" x="0" y="1" width="900" height="1000" filterUnits="userSpaceOnUse">
			<feOffset result="offset" dy="3" in="SourceAlpha"/>
			<feGaussianBlur result="blur"/>
			<feFlood result="flood" flood-opacity="0.2"/>
			<feComposite result="composite" operator="in" in2="blur"/>
			<feBlend result="blend" in="SourceGraphic"/>
			<feFlood result="flood-2" flood-color="#220237"/>
			<feComposite result="composite-2" operator="in" in2="SourceGraphic"/>
			<feBlend result="blend-2" in2="blend"/>
		</filter>
	</defs>
	<path id="Shape_111_copy" data-name="Shape 111 copy" class="cls-1" d="M0,75V1S4.2,11.8,25,21,62,35,62,50v82c0,15-16.2,19.8-37,29S0,181,0,181V75Z" transform="translate(0 -1)"/>
</svg>
<style> 
body {
	background-color: red;
}
</style>
