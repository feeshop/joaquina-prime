<?php include_once('header.php') ?> 
<?php include_once('menu.php') ?> 
<main class="intern">
  <section id="attractions" class="information">
    <div class="center-content">
      <h1>Atrações Para Todas as Idades</h1>
      <div class="centered">
        <p>
          Oferecemos a nossos clientes o que há de mais atual em termos de diversão e entretenimento, conjugado a atrações clássicas que não podem faltar na sua festa. Brinquedos novos e incríveis para todas as idades. Você conta com um espaço baby exclusivo para seu bebê, um momento de descanso e distração para os pequenininhos.
        </p>
        <p>
          Nosso salão de jogos é um lugar mágico para a garotada, composto por brinquedos de vários modelos. Aqui pais e filhos brincam juntos, sempre monitorados por nossos profissionais para que se divirtam com total segurança. Além disso, nossos brinquedos são inspecionados periodicamente, garantindo segurança e tranqüilidade a você e seus convidados.
        </p>
        <p>
          Seus baixinhos serão surprendidos com tanta diversão!
        </p>
        <p><b>Clique nos botões abaixo conheça nossas atrações:</b></p>
        <div class="attraction-grid">
          <button 
          data-gallery='["Super Brinquedão Aéreo", "images/brinquedao.gif", "images/brinquedao.gif", "images/forbabies.png", "images/reception.png"]' data-index="0">
          SUPER BRINQUEDAO AÉREO</button>
          <button data-gallery='["AIR GAME", "images/festa2.png", "images/festa2.png", "images/festa6.png", "images/gastronomia.jpg"]' data-index="1">AIR GAME</button>
          <button >ÁREA BABY TEMATIZADA</button>
          <button >ARENA LASER SHOT</button>
          <button >ARVORISMO COM TIROLESA</button>
          <button >CAMARIM</button>
          <button >ELEVADOR GIRATÓRIO</button>
          <button >KINECT</button>
          <button >LABAMBA</button>
          <button >MÁQUINA DE BOXE</button>
          <button >QUADRA DE FUTEBOL</button>
          <button >SIMULADOR DE BASQUETE</button>
          <button >SIMULADOR DE CORRIDA</button>
          <button >SIMULADOR DE FUTEBOL</button>
          <button >SIMULADOR SENINHA</button>
          <button >SUPER TOBOGÃ</button>
          <button >TOMBO LEGAL</button>
        </div>
      </div>
    </div>
  </section>
  <div class="lightbox">
    <div class="contain">
      <button class="close"><i class="fa fa-times"></i></button>
      <h2></h2>
      <button class="prev"></button>
      <div class="picture">
        <img src=""/>
      </div>
      <button class="next"></button>
      <ul>
      </ul>
    </div>
  </div>
</main>
<?php include_once('footer.php') ?> 
<script>
function listen() {
  if (window.innerWidth < 693) {
    $('.attraction-grid').slick({
      infinite: true, 
      autoplay: true, 
      autoplaySpeed: 2000,  
      arrows: false
    });
  }

  else{
    $('.attraction-grid').slick('unslick');
  }
};

listen();
$(window).on('resize', listen)

</script>